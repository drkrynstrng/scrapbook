#!/usr/bin/env bash

# Update Debian system

set -e

apt-get -y update
printf "\n"
apt-get -y upgrade
