#!/usr/bin/env bash

# Back up files with Kopia
# See ~/.kopiaignore for include/exclude list

set -e

printf "Backing up files...\n"

kopia repository connect filesystem --path /path/to/bkupdir
kopia snapshot create /path/to/dir
kopia repository disconnect

printf "Back up complete...\n"
