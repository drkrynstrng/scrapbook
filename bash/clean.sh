#!/usr/bin/env bash

# Clean Debian system

set -e

# Clean packages
apt-get -y autoremove --purge
printf "\n"
apt-get -y clean

# Clean system logs
journalctl --vacuum-time=30d
