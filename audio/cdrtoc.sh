#!/usr/bin/env bash

# Create cdrdao toc file from directory of flac files
# Run script in same directory as flac files

set -e

# Convert flac files to wav files
flac --totally-silent -d *.flac

# Create toc file based on wav files
echo "CD_DA" > cdr.toc
files=("*.wav")
for i in ${files[@]}; do
    echo "" >> cdr.toc
    echo "TRACK AUDIO" >> cdr.toc
    echo "FILE" "\"$PWD/$i\"" "0" >> cdr.toc
done
