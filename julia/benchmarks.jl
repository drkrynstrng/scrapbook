using InteractiveUtils
using BenchmarkTools
using BaseBenchmarks

versioninfo()

BaseBenchmarks.load!("io")
BaseBenchmarks.load!("linalg")
BaseBenchmarks.load!("micro")
BaseBenchmarks.load!("string")

results = run(BaseBenchmarks.SUITE; verbose = true)
BenchmarkTools.save("results.json", results)
