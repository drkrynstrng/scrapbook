# Install Julia packages

using Pkg

pkgs = [
    "PackageCompiler",
    "Revise",
    "BenchmarkTools",
    "Debugger",
    "Documenter",
    "Weave",
    "StatsBase",
    "StatsModels",
    "DataFrames",
    "DataFramesMeta",
    "Distributions",
    "MultivariateStats",
    "HypothesisTests",
    "Distances",
    "KernelDensity",
    "Clustering",
    "GLM",
    "TimeSeries",
    "CSV",
    "CairoMakie",
    "GLMakie"
]

Pkg.add(pkgs)
