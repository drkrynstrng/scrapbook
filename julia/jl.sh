#!/usr/bin/env bash

# Run Julia script with multiple threads
julia --threads 4 script.jl
