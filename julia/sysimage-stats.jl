# Create sysimage for statistical computing

# To create sysimage, run the following in the REPL:
# using PackageCompiler
# create_sysimage(["DataFrames", "DataFramesMeta", "StatsBase", "CSV", "GLMakie"], sysimage_path="stats.so", precompile_execution_file="sysimage-stats.jl")

# To use sysimage, run the following command:
# julia --sysimage stats.so

# Exercises for creating sysimage

using DataFrames

df = DataFrame(x = rand(100), y = rand(100))
cc = completecases(df)
nu = nonunique(df)
u = unique(df)
dm = dropmissing(df)
x = select(df, :x)
s = subset(df, :x => ByRow(<(.5)))

using DataFramesMeta

@chain df begin
    @subset :x .> .5
    @subset :y .< .5
    @orderby :x
    @transform :p = 2 * :x
end

using StatsBase

summarystats(df.x)
describe(df)

using CSV

CSV.write("test.csv", df)
df2 = CSV.read("test.csv", DataFrame)
rm("test.csv")

using GLMakie

density(df.x)
ecdfplot(df.x)
hist(df.x)
lines(df.x)
qqplot(df.x, df.y, qqline = :identity)
scatter(df.x, df.y)
