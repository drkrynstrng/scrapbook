#!/usr/bin/env bash

# Convert LaTeX document to PDF
# Provide path to file as argument

set -e

pdflatex $1
bibtex $1
pdflatex $1
pdflatex $1
