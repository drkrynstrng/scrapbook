#!/usr/bin/env bash

# Convert Markdown document to HTML

set -e

pandoc src/file.md \
  --standalone \
  --from=markdown \
  --to=html5 \
  --metadata=lang:en \
  --data-dir=web/site \
  --template=src/templates/page.html \
  --css=src/assets/styles/main.css \
  --output=public/file.html
